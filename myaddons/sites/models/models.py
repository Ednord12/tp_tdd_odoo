# -*- coding: utf-8 -*-

from odoo import models, fields, api


class sites(models.Model):
    _name = 'sites.sites'
    _description = 'Gestion completes des sites de CinePrex'

    name = fields.Char()
    address = fields.Text()
    salles_ids = fields.One2many('sites.salles', 'site_id', string='Salles')
    description = fields.Text()
    # value2 = fields.Float(compute="_value_pc", store=True)


class salles(models.Model):
    _name = 'sites.salles'
    _description = 'sites.salles'

    name = fields.Char()
    nbr_place = fields.Integer()
    description = fields.Text()
    site_id = fields.Many2one('sites.sites', string='Site')

    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100
